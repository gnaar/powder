module.exports = {
  collectCoverage: true,
  coveragePathIgnorePatterns: [
    '<rootDir>/node_modules/',
    '<rootDir>/src/js/i18n/(?!index|english)'
  ],
  coverageReporters: [ 'lcov', 'text' ],
  coverageThreshold: {
    global: {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100
    }
  },
  moduleNameMapper: {
    '\\.(jpe?g|png|gif|eot|otf|webp|svg|ttf|woff2?|mp4|webm|wav|mp3|m4a|aac|oga)$': '<rootDir>/src/__mocks__/file.js',
    '\\.(s?css|less)$': '<rootDir>/src/__mocks__/css.js'
  },
  roots: [
    '<rootDir>/src/js'
  ],
  setupFiles: [
    '<rootDir>/jest.setup.js'
  ],
  transformIgnorePatterns: [
    '<rootDir>/node_modules/(?!gnar-edge)'
  ]
};
