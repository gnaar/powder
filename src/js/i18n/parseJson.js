// export default json => _.mapValues(value =>

const re = /{{[^}]+}}/;

const parser = obj => [
  {
    test: a => _.isArray(a),
    result: a => (a.find(s => re.test(s)) ? param => a.map(s => s.replace(re, param)) : a)
  },
  {
    test: o => _.isObject(o) && 'plural' in o && 'singular' in o,
    result: o => count => (count === 1 ? o.singular : o.plural.replace(re, count))
  },
  { test: o => _.isObject(o), result: o => _.mapValues(o, parser) },
  { test: s => re.test(s), result: s => count => s.replace(re, count) },
  { test: () => true, result: s => s }
]
  .find(({ test }) => test(obj)).result(obj);

export default parser;
