import parseJson from './parseJson';
import rawEnglish from './english.json';

// [Project Gnar]: English is used for the values because webpack doesn't play nicely with non-ASCII filenames

export const ENGLISH = 'ENGLISH';
const FRENCH = 'FRENCH';
const SPANISH = 'SPANISH';
const CHINESE = 'CHINESE';
const KOREAN = 'KOREAN';
const SWEDISH = 'SWEDISH';

export const languages = [
  { text: 'English', value: ENGLISH },
  { text: 'Français', value: FRENCH },
  { text: 'Español', value: SPANISH },
  { text: '中文', value: CHINESE },
  { text: '한국어', value: KOREAN },
  { text: 'Svenska', value: SWEDISH }
];

export { parseJson };

export const english = parseJson(rawEnglish);

export default { [ENGLISH]: english };
