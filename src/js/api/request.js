import { delay } from 'redux-saga';
import { isLoggedIn, jwtDecode } from 'gnar-edge/es/jwt';
import { notifyError } from 'gnar-edge/es/notifications';
import moment from 'moment';

import { ExpiredTokenError } from 'errors';
import { SUBMIT_LOGOUT } from 'actions';
import { call, put, spawn } from 'redux-saga/effects';

const origins = JSON.parse(process.env.ORIGINS || '{}');

const camelCaseify = obj => _.transform(obj, (memo, val, key) => {
  memo[_.camelCase(key)] = _.isObject(val) ? camelCaseify(val) : val;
});

function* checkJwt(ms) {
  yield delay(ms);
  if (window.location.pathname !== '/login' && !isLoggedIn()) {
    const error = new ExpiredTokenError();
    yield notifyError(error.message);
    yield put({ type: SUBMIT_LOGOUT });
  }
}

export default function* request(service, path, method, body, { auth = false, toastErrors = true } = {}) {
  try {
    if (auth && path !== 'login' && !isLoggedIn()) {
      yield put({ type: SUBMIT_LOGOUT });
      throw new ExpiredTokenError();
    }
    const origin = origins[service] || '';
    const response = yield call(
      fetch,
      `${origin}/${service}/${path}`,
      _.omitBy({
        method,
        headers: auth && path !== 'login' ? { Authorization: `Bearer ${localStorage.getItem('jwt')}` } : null,
        body: body ? JSON.stringify(body) : null
      }, _.isNil)
    );
    const json = yield call([ response, 'json' ]);
    if (auth && (path !== 'login' || !json.error)) {
      const jwt = (response.headers.get('Authorization') || '').replace(/^Bearer /, '');
      if (!jwt) {
        yield put({ type: SUBMIT_LOGOUT });
        throw new ExpiredTokenError();
      }
      localStorage.setItem('jwt', jwt);
      const ms = moment.unix(jwtDecode(jwt).exp).diff();
      yield spawn(checkJwt, ms + 250);
    }
    if (json.error) {
      console.error(json.traceback || json.error);
      toastErrors && (yield notifyError(json.error));
    }
    return camelCaseify(json);
  } catch (error) {
    console.error(error);
    toastErrors && (yield notifyError(error.message));
    throw error;
  }
}
