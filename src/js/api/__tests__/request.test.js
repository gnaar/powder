import { applyMiddleware, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { drain } from 'gnar-edge';

import { call, takeEvery } from 'redux-saga/effects';
import { FetchMock } from 'mocks';

describe('api request', () => {
  beforeEach(() => {
    delete process.env.ORIGINS;
    jest.resetModules();
  });

  it('produces the correct url in development', drain(function* () {
    process.env.ORIGINS = JSON.stringify({ user: 'http://localhost:9400' });
    const request = require('api/request').default;  // eslint-disable-line global-require
    function* submitRequest() {
      yield call(request, 'user', 'test', 'GET');
    }
    function* watchResponse() {
      yield takeEvery('TEST', submitRequest);
    }
    const sagaMiddleware = createSagaMiddleware();
    const store = createStore(() => {}, applyMiddleware(sagaMiddleware));
    sagaMiddleware.run(watchResponse);
    const fetchMock = new FetchMock();
    yield store.dispatch({ type: 'TEST' });
    expect(fetchMock.fn).toHaveBeenCalledWith('http://localhost:9400/user/test', { method: 'GET' });
    fetchMock.deactivate();
  }));

  it('produces the correct url in production', drain(function* () {
    const request = require('api/request').default;  // eslint-disable-line global-require
    function* submitRequest() {
      yield call(request, 'user', 'test', 'GET');
    }
    function* watchResponse() {
      yield takeEvery('TEST', submitRequest);
    }
    const sagaMiddleware = createSagaMiddleware();
    const store = createStore(() => {}, applyMiddleware(sagaMiddleware));
    sagaMiddleware.run(watchResponse);
    const fetchMock = new FetchMock();
    yield store.dispatch({ type: 'TEST' });
    expect(fetchMock.fn).toHaveBeenCalledWith('/user/test', { method: 'GET' });
    fetchMock.deactivate();
  }));
});
